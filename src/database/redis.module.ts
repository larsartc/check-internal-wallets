import { createClient } from 'redis';
import { Global, Module } from '@nestjs/common';

const redisHost = process.env.REDIS_HOST || '167.71.145.172';
const redisPort: any = process.env.REDIS_PORT || 32408;
const redisPass = process.env.REDIS_PASS || 'w4yX4knJVE';

const factory = {
  provide: 'REDIS_CONNECTION',
  useFactory: async () => {
    return createClient({
      host: redisHost,
      port: redisPort,
      password: redisPass,
    });
  },
};

@Global()
@Module({
  providers: [factory],
  exports: [factory],
})
export class RedisModule {}

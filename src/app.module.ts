import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WalletModule } from './components';
import { RedisModule } from './database';

const pgHost = process.env.PG_HOST || '167.71.145.172';
const pgPort: number = parseInt(process.env.PG_PORT, 10) || 30432;
const pgUser = process.env.PG_USER || 'postgres';
const pgPass = process.env.PG_PASSWORD || 'HyRe0xw3Qx';
const pgDatebase = process.env.PG_DATABASE || 'postgres';
const pgSchema = process.env.PG_SCHEMA || 'arbwallet';

@Module({
  imports: [
    WalletModule,
    RedisModule,
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: pgHost,
      port: pgPort,
      username: pgUser,
      password: pgPass,
      database: pgDatebase,
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: false,
      logging: process.env.LOGGING === 'true',
      schema: pgSchema,
    }),
  ],
})
export class AppModule {}

import { Column, Entity, PrimaryColumn, BaseEntity } from 'typeorm';

@Entity('address')
export class Address extends BaseEntity {
  @PrimaryColumn()
  id: number;

  @Column({ type: 'int', nullable: false })
  invalid: number;

  @Column({ type: 'varchar', nullable: false })
  currency: string;

  // @todo fixing to the variable be userId but catch on database
  @Column({ type: 'int', nullable: false })
  // tslint:disable-next-line:variable-name
  user_id: number;

  @Column({ type: 'varchar', nullable: false })
  address: string;

  @Column({ type: 'int', nullable: false })
  balance: number;

  @Column({ type: 'varchar', nullable: false })
  time: string;
}

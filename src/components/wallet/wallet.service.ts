import {
  Injectable,
  BadRequestException,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Address } from '../../entity';
import { ValidateWalletDto } from './dto';
import { CoinService } from '../coin/coin.service';

@Injectable()
export class WalletService {
  constructor(
    private readonly coinService: CoinService,
    @InjectRepository(Address)
    private readonly addressRepository: Repository<Address>,
  ) {}

  async validateWallet(query: ValidateWalletDto): Promise<object> {
    const { currency, wallet } = query;
    await this.coinService.validateCoinAvailable({ coin: currency });

    const walletFound = await this.addressRepository.findOne({
      where: {
        address: wallet,
        currency,
      },
    });
    if (!walletFound) {
      return new NotFoundException('can`t find the wallet');
    }
    if (walletFound.invalid === 1) {
      throw new BadRequestException('wallet is invalid!');
    }
    return {
      success: true,
      data: {
        address: wallet,
        currency,
      },
    };
  }
}

import {
  Controller,
  Query,
  Get,
  UsePipes,
  ValidationPipe,
  Headers,
} from '@nestjs/common';
import { WalletService } from './wallet.service';
import { ValidateWalletDto } from './dto';

@Controller('/wallet')
export class WalletController {
  constructor(private readonly walletService: WalletService) {}

  @Get('/validate')
  @UsePipes(ValidationPipe)
  async validateWallet(@Query() query: ValidateWalletDto) {
    return await this.walletService.validateWallet(query);
  }
}

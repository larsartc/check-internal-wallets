import { IsNotEmpty } from 'class-validator';

export class ValidateWalletDto {
  @IsNotEmpty({ message: 'Campo obrigatório' })
  currency: string;
  @IsNotEmpty({ message: 'Campo obrigatório' })
  wallet: string;
}

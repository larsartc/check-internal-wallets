import { Module } from '@nestjs/common';
import { Address } from '../../entity';
import { WalletService } from './wallet.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WalletController } from './wallet.controller';
import { CoinService } from '../coin/coin.service';

@Module({
  imports: [TypeOrmModule.forFeature([Address])],
  providers: [WalletService, CoinService],
  controllers: [WalletController],
})
export class WalletModule {}

import { Module } from '@nestjs/common';
import { CoinService } from './coin.service';
import { RedisModule } from '../../database/redis.module';
@Module({
  imports: [RedisModule],
  providers: [CoinService],
})
export class CoinModule {}

import { Injectable, Inject, BadRequestException } from '@nestjs/common';
import { RedisClient } from 'redis';
import { ICoin } from '../../interface/ICoin';

@Injectable()
export class CoinService {
  constructor(
    @Inject('REDIS_CONNECTION')
    private readonly redisClient: RedisClient,
  ) {}

  getRedisCoins(): Promise<string[]> {
    return new Promise((resolve, reject) => {
      this.redisClient.lrange('avaiable_coins', 0, -1, (err, value) => {
        if (err) {
          reject(err);
        }
        resolve(value);
      });
    });
  }

  private getRedisCoinsConfigurations(coin: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.redisClient.hgetall(`coin_${coin}`, (err, result) => {
        if (err) {
          reject('ERROR FIND COIN');
        }
        resolve(result);
      });
    });
  }

  async validateCoinAvailable({ coin }) {
    let getCoins;
    let getCoin: ICoin;

    try {
      getCoins = await this.getRedisCoins();
    } catch (err) {
      throw new BadRequestException(err);
    }

    if (!getCoins.includes(coin.toLowerCase())) {
      throw new BadRequestException('Invalid currency');
    }

    try {
      getCoin = await this.getRedisCoinsConfigurations(coin.toLowerCase());
    } catch (err) {
      throw new BadRequestException(err);
    }

    if (parseFloat(getCoin.active) === 0) {
      throw new BadRequestException('Invalid currency');
    }

    return {
      success: true,
      data: getCoin,
    };
  }
}

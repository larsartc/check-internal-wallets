FROM node:10-alpine
WORKDIR /app

RUN apk update && apk add yarn python g++ make && rm -rf /var/cache/apk/*

RUN chown node /app
USER node

COPY package.json yarn.lock ./
RUN yarn install --production=false --frozen-lockfile
COPY . .
RUN yarn run build

EXPOSE 3000

CMD ["node", "dist/main.js"]
